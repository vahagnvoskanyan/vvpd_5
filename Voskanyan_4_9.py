"""
Восканян Юрий Вачикович
Вариант 9, группа: КИ22-17/1б
Алгоритм сортирует список алгоритмом Шелла.
Список можно заполнить как вручную, так и рандомными числами
Для удобства добавлено меню

"""

from random import randint


def input_validation(message: str) -> bool or str:
    """ Перевод строки в целое число

    Функция принимает строку, если она состоит только из целых чисел,
    то возвращается целое число, в противном случае False

    Args:
        message:    Строка, введенная пользователем

    Returns:
        целое число,либо False

    Raises:
        ValueError

    Examples:
        >>> input_validation('Vvpd')
        False
        >>> input_validation('1024')
        1024
        >>> input_validation('1024d')
        False

    """
    try:
        meaning = int(input(message))
        return meaning
    except ValueError:
        return False


def shell_sort(array: list) -> list:
    """ Сортировка алгоритмом Шелла

    Функция на входе принимает список из целых чисел,
    и сортирует этот список в порядке возрастания

    Args:
        array:  Неотсортированный список из целых чисел

    Returns:
        Отсортированный список в порядке возрастания

    Examples:
        >>> shell_sort([-56, -872, -302, -196, 536, -845, 1018, 1018])
        [-872, -845, -302, -196, -56, 536, 1018, 1018]
        >>> shell_sort([11, 31, 78, 63, 71, 101, 34, 78])
        [11, 31, 34, 63, 71, 78, 78, 101]
        >>> shell_sort([42, 42, 42])
        [42, 42, 42]

    """
    step = len(array) // 2
    while step > 0:
        for i in range(step, len(array)):
            k = i
            d = k - step
            while d >= 0 and array[d] > array[k]:
                array[d], array[k] = array[k], array[d]
                k = d
                d = k - step
        step = step // 2
    return array


def list_completion(array: list) -> list:
    """ Заполнение списка

    Функция заполняет список целыми числами.
    Заполнить можно вручную, либо случайными числами, на усмотрение пользователя.
    Функция устойчива к значениям, т.е при вводе не целого числа,
    будет выведено соответствующее сообщение

    Args:
        array:  Пустой список

    Returns:
        Заполненный список целыми числами
    """
    msg = (
        '1)Заполнить список рандомными числами\n'
        '2)Заполнить список вручную:\n'
    )
    while requests := input(msg):
        if requests in ('1', '2'):
            break
        else:
            print("Было введеное некорректное значение.")
    if requests == '1':
        while True:
            size = input_validation("Введите размер списка: ")
            a = input_validation("Введите левую границу: ")
            b = input_validation("Введите правую границу: ")
            if all((size, a, b)):
                break
            else:
                print("Введите целые числа!")
        while size > 0:
            x = randint(a, b)
            array.append(x)
            size -= 1

    elif requests == '2':
        while True:
            amount_of_elements = input_validation("Сколько элементов вы хотите добавить?: ")
            if amount_of_elements:
                break
            else:
                print("Введите целое число!")

        while True:
            if len(array) == amount_of_elements:
                break
            element = input_validation("Введите элемент: ")
            if element:
                array.append(element)
            else:
                print("Введите целое число!")

    return array


def main():
    array = []
    message = (
        'Для удобства, меню выводится 1 раз\n'
        'Для повторного вывода меню введите --> menu\n'
        '0 --> Выход из программы\n'
        '1 --> Заполнить список\n'
        '2 --> Отсортировать список\n'
        '3 --> Вывести список\n'
        '4 --> Очистить список: '
    )
    print(message)
    while True:
        while menu := input("Выберите действие: "):
            if menu in ('menu', '0', '1', '2', '3', '4'):
                break
            else:
                print("Было введено некорректное значение")
                continue
        dictionary_menu = {
            '1': list_completion,
            '2': shell_sort,
        }
        if menu in dictionary_menu:
            array = dictionary_menu[menu](array)
        else:
            if menu == 'menu':
                print(message)
            elif menu == '0':
                break
            elif menu == '3':
                print(array)
            elif menu == '4':
                array.clear()
            else:
                print("Неизвестная команда")


if __name__ == '__main__':
    main()
